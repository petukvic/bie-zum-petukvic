package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.example.Constants.*;

public class Point {
    int x;
    int y;

    Point prev=null;

    int value =0;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public List<Point> getNeighborsNotVisitedOrOpened(List<List<Character>> maze) {
        List <Point> neighbourPoints = new ArrayList<>();
        if (maze.get(y).get(x - 1) == NOTVISITED || maze.get(y).get(x - 1) == OPEN) {
            neighbourPoints.add(new Point(x - 1, y));
        }
        if (maze.get(y).get(x + 1) == NOTVISITED || maze.get(y).get(x + 1) == OPEN) {
            neighbourPoints.add(new Point(x + 1, y));
        }
        if (maze.get(y - 1).get(x) == NOTVISITED || maze.get(y-1).get(x) == OPEN) {
            neighbourPoints.add(new Point(x, y - 1));
        }
        if (maze.get(y + 1).get(x) == NOTVISITED || maze.get(y+1).get(x) == OPEN) {
            neighbourPoints.add(new Point(x, y + 1));
        }
        return neighbourPoints;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
