package org.example;

public class Constants {

    static final Character WALL = 'X';
    static final Character START = 'S';
    static final Character END = 'E'; static final char OPEN = 'o';
    static final Character CLOSED = '#';
    static final Character NOTVISITED = ' ';

    static final Point UNDETERMINED_POINT = new Point(0,0);
}
