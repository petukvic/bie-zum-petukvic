package org.example;

public class Coin {

    Point position;

    boolean stored = false;

    boolean taken = false;

    boolean reserved = false;

    public Coin(Point position) {
        this.position = position;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
}
