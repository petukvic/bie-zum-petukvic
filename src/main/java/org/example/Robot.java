package org.example;

import java.util.Stack;

public class Robot {

    Coin coin = null;
    Character name;
    Point position;

    Stack<Point> nextSteps = new Stack<>();

    Point target = null;

    public Robot(Point position, Character name) {
        this.position = position;
        this.name = name;
    }

    public Point getPosition() {
        return position;
    }
    public Point getTarget() {
        return target;
    }

    public void setTarget(Point target) {
        this.target = target;
    }

    public void changePosition() {
        position = nextSteps.pop();
        if (position.equals(target)) {
            if (coin != null) {
                if (coin.taken != true) {
                    coin.taken =true;
                } else {
                    coin.stored = true;
                    coin=null;
                }
            }
            target = null;
        }
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }
}
