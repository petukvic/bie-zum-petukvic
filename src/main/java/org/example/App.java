package org.example;

import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File(args[0]);
        Scanner scanner = new Scanner(file);
        List<Robot> robots = new ArrayList<>();
        List<Coin> coins = new ArrayList<>();

        Point target = null;
        List<List<Character>> maze = new ArrayList<>();
        int i = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            List<Character> newArrayList = new ArrayList<>();
            int j = 0;
            for (Character c : line.toCharArray()) {
                if (c >= '1' && c <= '9') {
                    Point robotPosition = new Point(j, i);
                    robots.add(new Robot(robotPosition, c));
                    newArrayList.add(' ');
                } else if (c == 'T') {
                    target = new Point(j, i);
                    newArrayList.add(' ');
                } else if (c.equals('c')) {
                    coins.add(new Coin(new Point(j, i)));
                    newArrayList.add(' ');
                } else
                    newArrayList.add(c);
                j++;
            }
            maze.add(newArrayList);
            i++;
        }
        boolean finish = false;
        for (Robot robot : robots) {
            Coin robotCoin = Algorithms.findClosestCoin(coins, robot.getPosition());
            if (robotCoin != null) {
                robot.setTarget(robotCoin.getPosition());
                robot.setCoin(robotCoin);
                Stack<Point> points = new Stack<>();
                points.addAll(Algorithms.aStar(maze, robot.getPosition(), robot.getTarget()));
                Collections.reverse(points);
                robot.nextSteps = points;
            }
        }
        while (coinsNotStored(coins)) {
            printMaze(maze, coins, robots, target);
            for (Robot robot : robots) {
                if (robot.getTarget() != null) {
                    robot.changePosition();
                }
                if (robot.coin != null && robot.getTarget() == null) {
                    robot.setTarget(target);
                    Stack<Point> points = new Stack<>();
                    points.addAll(Algorithms.aStar(maze, robot.getPosition(), robot.getTarget()));
                    Collections.reverse(points);
                    robot.nextSteps = points;
                } else if (robot.coin == null && robot.getTarget() == null) {
                    Coin robotCoin = Algorithms.findClosestCoin(coins, robot.getPosition());
                    if (robotCoin == null) {
                        continue;
                    } else {
                        robot.setTarget(robotCoin.getPosition());
                        robot.setCoin(robotCoin);
                    }
                    if (robot.getTarget() != null) {
                        Stack<Point> points = new Stack<>();
                        points.addAll(Algorithms.aStar(maze, robot.getPosition(), robot.getTarget()));
                        Collections.reverse(points);
                        robot.nextSteps = points;
                    }
                }
            }
        }
        printMaze(maze, coins, robots, target);
    }

    private static void printMaze(List<List<Character>> maze, List<Coin> coins, List<Robot> robots, Point target) {
        List<List<Character>> printMaze = new ArrayList<>();
        for (List<Character> characters : maze) {
            printMaze.add(new ArrayList<>(characters));
        }
        for (Coin coin : coins) {
            if (!coin.taken && !coin.stored)
                printMaze.get(coin.position.y).set(coin.position.x, 'c');
        }
        printMaze.get(target.y).set(target.x, 'T');
        for (Robot robot : robots) {
            printMaze.get(robot.position.y).set(robot.position.x, robot.name);
        }
        for (List<Character> characters : printMaze) {
            for (Character character : characters) {
                System.out.print(character);
            }
            System.out.println();
        }

    }

    private static boolean coinsNotStored(List<Coin> coins) {
        return coins.stream().anyMatch(coin -> !coin.stored);
    }


}
