package org.example;

import java.util.*;

public class Algorithms {

    private static void printTheMaze(List<List<Character>> maze, Point start, Point end) {
        for (List<Character> characters : maze) {
            for (Character character : characters) {
                System.out.print(character);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static List<Point> aStar(List<List<Character>> maze, Point start, Point end) {
        PriorityQueue<Point> queue = new PriorityQueue<>(Comparator.comparingInt(a -> heuristic2(a, end)));
        List<Point> path = new ArrayList<>();
        List<List<Character>> newMaze = new ArrayList<>();
        for (List<Character> characters : maze) {
            newMaze.add(new ArrayList<>(characters));
        }
        queue.offer(start);
        newMaze.get(start.y).set(start.x, Constants.OPEN);
        while (!queue.isEmpty()) {
            Point p = queue.poll();
            newMaze.get(p.y).set(p.x, Constants.CLOSED);
            if (p.equals(end)) {
                System.out.print("path is found\n");
                return path;
            }
            for (Point neighbour: p.getNeighborsNotVisitedOrOpened(newMaze)) {
                if (newMaze.get(neighbour.y).get(neighbour.x) != Constants.OPEN || neighbour.value > p.value + 1) {
                    neighbour.prev = p;
                }
                neighbour.value = p.value + 1;
                if (newMaze.get(neighbour.y).get(neighbour.x) != Constants.OPEN) {
                    queue.offer(neighbour);
                } else {
                    for (Point point : queue) {
                        if (point.equals(neighbour)) {
                            point.value = neighbour.value;
                        }
                    }
                }
                if (neighbour.equals(end)) {
                    path.add(neighbour);
                    System.out.print("path is found\n");
                    Point curr = p;
                    while (!curr.equals(start)) {
                        path.add(curr);
                        curr = curr.prev;
                    }
//                    printTheMaze(maze, start, end);

                    Collections.reverse(path);
                    System.out.println("Path: " + path);
                    return path;
                }
                newMaze.get(neighbour.y).set(neighbour.x, Constants.OPEN);
            }
        }
        printTheMaze(newMaze, start, end);
        System.out.print("path is not found\n");
        return path;
    }

    public static Coin findClosestCoin(List<Coin> coins, Point position) {
        int minimalHeuristic = 100000;
        Coin targetCoin = null;
        for (Coin coin : coins) {
            if (heuristic(position,coin.getPosition())< minimalHeuristic && coin.isReserved() == false) {
                targetCoin = coin;
                minimalHeuristic = heuristic(position,coin.getPosition());
            }
        }
        if (targetCoin != null)
            targetCoin.setReserved(true);
        return targetCoin;
    }


    private static int heuristic(Point a, Point b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }

    private static int heuristic2(Point a, Point b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + a.value;
    }
}
